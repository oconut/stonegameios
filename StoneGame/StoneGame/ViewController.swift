//
//  ViewController.swift
//  StoneGame
//
//  Created by 이준규 on 2014. 10. 31..
//  Copyright (c) 2014년 이준규. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        for i in 0...11 {
            var button = UIButton(frame: CGRectZero)
            button.setTitle("Button", forState: .Normal)
            button.layer.cornerRadius = 10.0
            button.backgroundColor = UIColor.brownColor()
            var x = i%3
            var y = i/3
            button.frame = CGRectMake(10.0+CGFloat(x)*100.0, 80.0+CGFloat(y)*100.0, 90.0, 90.0);
            button.addTarget(self, action: "buttonAction:", forControlEvents: UIControlEvents.TouchUpInside)
            button.tag = i
            view.addSubview(button)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func buttonAction(sender: UIButton) {
        println("Button tag : \(sender.tag)")
    }
}

